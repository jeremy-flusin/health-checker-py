FROM python:3.8

CMD ["python", "-m", "http.server", "--directory", "/html", "8080"]
